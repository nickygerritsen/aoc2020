import AoCShared

enum Instruction {
    case acc(value: Int)
    case jmp(value: Int)
    case nop(value: Int)
    
    static func parseLine(line: String.SubSequence) -> Instruction? {
        let parts = line.split(separator: " ")
        let numberPart = parts[1]
        let isPositive = numberPart.starts(with: "+")
        let number = numberPart[numberPart.index(after: numberPart.startIndex)...]
        guard let n = Int(number) else {
            return nil
        }
        let nn = isPositive ? n : (-1 * n)
        switch String(parts[0]) {
        case "acc":
            return .acc(value: nn)
        case "jmp":
            return .jmp(value: nn)
        case "nop":
            return .nop(value: nn)
        default:
            return nil
        }
    }
}

class Program {
    var acc = 0
    var ip = 0
    let instructions: [Instruction]
    
    init(instructions: [Instruction]) {
        self.instructions = instructions
    }
    
    func runStep() {
        let instruction = instructions[ip]
        switch instruction {
        case let .acc(value: n):
            acc += n
            ip += 1
        case let .jmp(value: n):
            ip += n
        case .nop:
            ip += 1
        }
    }
}

struct Day8: AdventOfCode {
    typealias InputData = [Instruction]
    
    func parse(input: String) -> [Instruction]? {
        input.split(whereSeparator: \.isNewline).compactMap { Instruction.parseLine(line: $0) }
    }
    
    // Returns whether it stopped and the value of the accumulator
    func run(data: [Instruction]) -> (Bool, Int) {
        var ran: Set<Int> = []
        let p = Program(instructions: data)
        while true {
            if ran.contains(p.ip) {
                return (false, p.acc)
            } else if p.ip >= p.instructions.count {
                break
            }
            
            ran.insert(p.ip)
            
            p.runStep()
        }
        
        return (true, p.acc)
    }
    
    func part1(data: [Instruction]) {
        print(run(data: data).1)
    }
    
    func part2(data: [Instruction]) {
        for i in 0..<data.count {
            switch data[i] {
            case .acc:
                // Nothing
                break
            case let .jmp(value: n):
                var copy = data
                copy[i] = .nop(value: n)
                let r = run(data: copy)
                if r.0 {
                    print(r.1)
                }
            case let .nop(value: n):
                var copy = data
                copy[i] = .jmp(value: n)
                let r = run(data: copy)
                if r.0 {
                    print(r.1)
                }
            }
        }
    }
}

Day8().solve()
