import AoCShared

struct Day6: AdventOfCode {
    typealias InputData = [[Set<Character>]]
    
    func parse(input: String) -> [[Set<Character>]]? {
        var result: [[Set<Character>]] = []
        var item: [Set<Character>] = []
        for line in input.split(separator: "\n", omittingEmptySubsequences: false) {
            if line == "" {
                result.append(item)
                item = []
            } else {
                let chars = Set<Character>(line)
                item.append(chars)
            }
        }
        
        if !item.isEmpty {
            result.append(item)
        }
        
        return result
    }
    
    func part1(data: [[Set<Character>]]) {
        print(
            data
                .map { $0.reduce(Set<Character>()) { $0.union($1) }.count }
                .reduce(0) { $0 + $1 }
        )
    }
    
    func part2(data: [[Set<Character>]]) {
        print(
            data
                .map { $0.reduce(Set<Character>($0[0])) { $0.intersection($1) }.count }
                .reduce(0) { $0 + $1 }
        )
    }
}

Day6().solve()
