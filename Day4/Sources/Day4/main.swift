import AoCShared
import Foundation

struct Day4: AdventOfCode {
    typealias InputData = [[String: String]]

    func parse(input: String) -> InputData? {
        var result = [[String: String]]()
        var item = [String: String]()
        for line in input.split(omittingEmptySubsequences: false, whereSeparator: \.isNewline) {
            if line == "" {
                result.append(item)
                item = [String: String]()
            } else {
                for part in line.split(whereSeparator: \.isWhitespace) {
                    let keyValue = part.split(separator: ":")
                    item[String(keyValue[0])] = String(keyValue[1])
                }
            }
        }

        if !item.isEmpty {
            result.append(item)
        }

        return result
    }

    func validPart1(data: InputData) -> InputData {
        let required: Set<String> = [
            "byr",
            "iyr",
            "eyr",
            "hgt",
            "hcl",
            "ecl",
            "pid",
        ]

        return data.filter { item in
            required.intersection(Set(item.keys)) == required
        }
    }

    func part1(data: InputData) {
        print(validPart1(data: data).count)
    }

    func part2(data: InputData) {
        print(validPart1(data: data).filter { item in
            item.allSatisfy { key, value in
                isValid(field: key, value: value)
            }
        }.count)
    }

    func isValid(field: String, value: String) -> Bool {
        switch field {
        case "byr":
            guard let val = Int(value) else {
                return false
            }
            return val >= 1920 && val <= 2002
        case "iyr":
            guard let val = Int(value) else {
                return false
            }
            return val >= 2010 && val <= 2020
        case "eyr":
            guard let val = Int(value) else {
                return false
            }
            return val >= 2020 && val <= 2030
        case "hgt":
            if value.reversed().starts(with: "mc") {
                guard let val = Int(value.dropLast(2)) else {
                    return false
                }
                return val >= 150 && val <= 193
            } else if value.reversed().starts(with: "ni") {
                guard let val = Int(value.dropLast(2)) else {
                    return false
                }
                return val >= 59 && val <= 76
            } else {
                return false
            }
        case "hcl":
            return value.range(of: #"^#[a-f0-9]{6}$"#, options: .regularExpression) != nil
        case "ecl":
            let allowed: Set<String> = [
                "amb",
                "blu",
                "brn",
                "gry",
                "grn",
                "hzl",
                "oth",
            ]
            return allowed.contains(value)
        case "pid":
            return value.range(of: #"^[0-9]{9}$"#, options: .regularExpression) != nil
        default:
            return true
        }
    }
}

Day4().solve()