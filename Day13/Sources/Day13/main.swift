import AoCShared

struct BusInfo {
    let currentTime: Int
    let ids: [Int]
}
struct Day13: AdventOfCode {
    typealias InputData = BusInfo
    
    func parse(input: String) -> BusInfo? {
        let lines = input.split(whereSeparator: \.isNewline)
        guard let currentTime = Int(String(lines[0])) else {
            return nil
        }
        let ids = lines[1].split(separator: ",").map { (item: String.SubSequence) -> Int in
            if let n = Int(String(item)) {
                return n
            } else {
                return 1
            }
        }
        return BusInfo(currentTime: currentTime, ids: ids)
    }
    
    func part1(data: BusInfo) {
        let answers = data.ids
            .filter { $0 > 1 }
            .map { (id: Int) -> (id: Int, dist: Int) in
                let m = data.currentTime % id
            let a = id - m
            return (id: id, dist: a)
        }
        if let m = answers.min(by: { (a, b) -> Bool in
            a.dist < b.dist
        }) {
            print(m.id * m.dist)
        }
    }
    
    func part2(data: BusInfo) {
        let ids = data.ids
        var increment = ids[0] // The number to increase the loop with every time
        var indexToCheck = 1 // This is the number we want to verify
        var answer = increment // Here we store the possible answer
        
        while indexToCheck < ids.count {
            // Check if the number we want to check matches the formula
            if (answer + indexToCheck) % ids[indexToCheck] == 0 {
                // Yes it does. This means we also increment by the new number, meaning we multiply the increment with it
                increment *= ids[indexToCheck]
                // And we move to the next index
                indexToCheck += 1
            } else {
                // We know nothing, so check the next number
                answer += increment
            }
        }
        
        print(answer)
    }
}

Day13().solve()
