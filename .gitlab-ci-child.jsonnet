local day_part(day) = {
  ["run day " + day]: {
    stage: "run",
    before_script: [
      "cd Day" + day + " && swift build -c release && cd .."
    ],
    script: [
      "cd Day" + day + " && swift run -c release && cd .."
    ],
    rules: [
      {
        changes: [
          '.gitlab-ci.yml',
          '.gitlab-ci-child.jsonnet',
          'AoCShared/**/*',
          'Day' + day + '/**/*'
        ],
      },
    ],
  }
} + (if day > 1 then day_part(day - 1) else {});

{
  image: "swift:5.3-focal",
  workflow: {
    rules: [{when: "always"}]
  },
  stages: ["run"],
} + day_part(DAY)
