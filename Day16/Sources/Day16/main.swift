import AoCShared
import Regex

struct FieldSpec {
    let name: String
    let firstRange: (low: Int, high: Int)
    let secondRange: (low: Int, high: Int)
    
    func isValid(_ value: Int) -> Bool {
        return value >= firstRange.low && value <= firstRange.high ||
            value >= secondRange.low && value <= secondRange.high
    }
}

struct Ticket {
    let fieldData: [Int]
}

struct TicketData {
    let fieldSpecs: [FieldSpec]
    let yourTicket: Ticket
    let nearbyTickets: [Ticket]
}

struct Day16: AdventOfCode {
    typealias InputData = TicketData
    
    func parse(input: String) -> TicketData? {
        var fieldSpecs: [FieldSpec] = []
        var yourTicket: Ticket? = nil
        var nearbyTickets: [Ticket] = []
        
        let specRegex = Regex(#"^(.*): (\d+)-(\d+) or (\d+)-(\d+)$"#)
        let yourTicketText = "your ticket:"
        let nearbyTicketsText = "nearby tickets:"
        var inYourTicket = false
        var inNearbyTickets = false
        
        for line in input.split(whereSeparator: \.isNewline) {
            if line == yourTicketText {
                inYourTicket = true
                continue
            } else if line == nearbyTicketsText {
                inNearbyTickets = true
                inYourTicket = false
                continue
            }
            
            if inYourTicket {
                let fieldData = line.split(separator: ",").compactMap { Int(String($0)) }
                yourTicket = Ticket(fieldData: fieldData)
            } else if inNearbyTickets {
                let fieldData = line.split(separator: ",").compactMap { Int(String($0)) }
                nearbyTickets.append(Ticket(fieldData: fieldData))
            } else {
                if let captures = specRegex.firstMatch(in: String(line))?.captures {
                    if let name = captures[0],
                       let firstRangeLow = captures[1],
                       let firstRangeHigh = captures[2],
                       let secondRangeLow = captures[3],
                       let secondRangeHigh = captures[4],
                       let firstRangeLowValue = Int(firstRangeLow),
                       let firstRangeHighValue = Int(firstRangeHigh),
                       let secondRangeLowValue = Int(secondRangeLow),
                       let secondRangeHighValue = Int(secondRangeHigh) {
                        let firstRange = (low: firstRangeLowValue, high: firstRangeHighValue)
                        let secondRange = (low: secondRangeLowValue, high: secondRangeHighValue)
                        fieldSpecs.append(FieldSpec(name: name, firstRange: firstRange, secondRange: secondRange))
                    }
                }
            }
        }
        
        if let yourTicket = yourTicket {
            return TicketData(fieldSpecs: fieldSpecs, yourTicket: yourTicket, nearbyTickets: nearbyTickets)
        }
        
        return nil
    }
    
    func part1(data: TicketData) {
        print(
            data
                .nearbyTickets
                .map { nearbyTicket in
                    nearbyTicket.fieldData.filter { ticketValue in
                        !data.fieldSpecs.contains { $0.isValid(ticketValue) }
                    }
                }
                .reduce([], +)
                .reduce(0, +)
        )
    }
    
    func part2(data: TicketData) {
        // First, get the tickets that can be valid
        let ticketsToConsider = data
            .nearbyTickets
            .filter { ticket in
                ticket.fieldData.allSatisfy { ticketValue in
                    data.fieldSpecs.contains { $0.isValid(ticketValue) }
                }
            }
        
        // Now determine which item can go to which spot, by removing places it doesn't fit
        var possibleSepcs: [Int: Set<String>] = [:]
        for i in 0..<data.yourTicket.fieldData.count {
            possibleSepcs[i] = Set<String>(data.fieldSpecs.map { $0.name} )
        }
        
        for ticket in ticketsToConsider {
            for (pos, value) in ticket.fieldData.enumerated() {
                for fieldSpec in data.fieldSpecs {
                    if !fieldSpec.isValid(value) {
                        var possible = possibleSepcs[pos]!
                        possible.remove(fieldSpec.name)
                        possibleSepcs[pos] = possible
                    }
                }
            }
        }
        
        // Now we are left with spots, assign items to their spots, starting with the item
        // with the least amount of spots available. This will be only one.
        // Next go to the one that allows 2 spots, then 3, etc.
        // The data is build up in such a way that this works.
        var assignment: [String: Int] = [:]
        var product = 1
        for (key, value) in possibleSepcs.sorted(by: { $0.value.count < $1.value.count }) {
            for possible in value {
                if assignment[possible] == nil {
                    assignment[possible] = key
                    if possible.starts(with: "departure") {
                        product *= data.yourTicket.fieldData[key]
                    }
                }
            }
        }
        print(product)
    }
}

Day16().solve()
