import AoCShared

struct Entity {
    let publicKey: Int
    var n = 1
    var loopSize = 0
    
    var solved: Bool {
        n == publicKey
    }
    
    mutating func loop() {
        n = (n * 7) % 20201227
        loopSize += 1
    }
}

struct Day25: AdventOfCode {
    typealias InputData = (card: Entity, door: Entity)
    
    func parse(input: String) -> (card: Entity, door: Entity)? {
        let lines = input.split(whereSeparator: \.isNewline)
        if let card = Int(lines[0]),
           let door = Int(lines[1]) {
            return (card: Entity(publicKey: card), door: Entity(publicKey: door))
        }
        
        return nil
    }
    
    func part1(data: (card: Entity, door: Entity)) {
        var card = data.card
        var door = data.door
        while !(card.solved || door.solved) {
            card.loop()
            door.loop()
        }
        
        let ls = card.solved ? card.loopSize : door.loopSize
        let pk = card.solved ? door.publicKey : card.publicKey
        var enc = 1
        for _ in 1...ls {
            enc = (enc * pk) % 20201227
        }
        print(enc)
    }
    
    func part2(data: (card: Entity, door: Entity)) {
        print(String(repeating: "⭐️", count: 50))
    }
}

Day25().solve()
