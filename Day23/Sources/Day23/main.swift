import AoCShared

class Node {
    var next: Node? = nil
    let item: Int
    
    init(item: Int) {
        self.item = item
    }
}

extension Node: Equatable {
    static func == (lhs: Node, rhs: Node) -> Bool {
        return lhs.item == rhs.item
    }
}

class Game {
    var currentCup: Node
    var cupMapping: [Node?]
    
    init(currentCup: Node, cnt: Int) {
        self.currentCup = currentCup
        cupMapping = Array(repeating: nil, count: cnt)
        cupMapping[currentCup.item - 1] = currentCup
        var cup = currentCup.next!
        while cup != currentCup {
            cupMapping[cup.item - 1] = cup
            cup = cup.next!
        }
    }
    
    init(currentCup: Node, cupMapping: [Node]) {
        self.currentCup = currentCup
        self.cupMapping = cupMapping
    }
    
    func play(moveCount: Int) {
        for _ in 1...moveCount {
            let pickUp = [self.currentCup.next!, self.currentCup.next!.next!, self.currentCup.next!.next!.next!]
            let pickupSet = Set(pickUp.map { $0.item })
            self.currentCup.next = self.currentCup.next?.next?.next?.next
            var destination = self.currentCup.item - 1
            if destination == 0 {
                destination = self.cupMapping.count
            }
            while pickupSet.contains(destination) {
                destination -= 1
                if destination == 0 {
                    destination = self.cupMapping.count
                }
            }
            let destinationCup = self.cupMapping[destination - 1]!
            let currentNext = destinationCup.next
            destinationCup.next = pickUp[0]
            pickUp[2].next = currentNext
            self.currentCup = self.currentCup.next!
        }
    }
}

extension Game: CustomDebugStringConvertible {
    var debugDescription: String {
        var output = ""
        output += "(\(currentCup.item)) "
        var cup = currentCup.next!
        while cup != currentCup {
            output += "\(cup.item) "
            cup = cup.next!
        }
        return output
    }
}

struct Day23: AdventOfCode {
    typealias InputData = (Game, Game)
    
    func getNodes(input: String, extendedTo: Int? = nil) -> [Node] {
        var nodes = input
            .trimmingCharacters(in: .whitespaces)
            .compactMap { Int(String($0)) }
            .map { Node(item: $0) }
        if let extendedTo = extendedTo {
            for i in nodes.count..<extendedTo {
                nodes.append(Node(item: i + 1))
            }
        }
        for i in 0..<nodes.count {
            if i == nodes.count - 1 {
                nodes[i].next = nodes[0]
            } else {
                nodes[i].next = nodes[i + 1]
            }
        }
        
        return nodes
    }
    
    func parse(input: String) -> (Game, Game)? {
        let nodes1 = getNodes(input: input)
        let nodes2 = getNodes(input: input, extendedTo: 1000000)
        return (Game(currentCup: nodes1[0], cnt: 9), Game(currentCup: nodes2[0], cnt: 1000000))
    }
    
    func part1(data: (Game, Game)) {
        let game = data.0
        game.play(moveCount: 100)
        
        let cupOne = game.cupMapping[0]!
        var output = ""
        var cup = cupOne.next!
        repeat {
            output += "\(cup.item)"
            cup = cup.next!
        } while cup != cupOne
        
        print(output)
    }
    
    func part2(data: (Game, Game)) {
        let game = data.1
        game.play(moveCount: 10000000)

        let cupOne = game.cupMapping[0]!
        print(cupOne.next!.item * cupOne.next!.next!.item)
    }
}

Day23().solve()
