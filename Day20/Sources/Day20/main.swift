import AoCShared
import Regex

struct Tile: Hashable {
    let id: Int
    let data: [String]
    var topBorder: String {
        data.first!
    }
    var rightBorder: String {
        String(data.map { $0.last! })
    }
    var bottomBorder: String {
        data.last!
    }
    var leftBorder: String {
        String(data.map { $0.first! })
    }
    var borders: [String] {
        return [topBorder, rightBorder, bottomBorder, leftBorder]
    }
    var allBorders: [String] {
        borders + borders.map { String($0.reversed()) }
    }
    var flippedVert: Tile {
        return Tile(id: id, data: data.reversed())
    }
    var flippedHort: Tile {
        return Tile(id: id, data: data.map { String($0.reversed()) })
    }
    var rotated: Tile {
        let oldData = data.map { [Character]($0) }
        let line = [Character](repeating: ".", count: data.count)
        var newData = [[Character]](repeating: line, count: data.count)
        for i in 0..<data.count {
            for j in 0..<data.count {
                newData[i][j] = oldData[data.count - 1 - j][i]
            }
        }
        return Tile(id: id, data: newData.map { String($0) })
    }
    var allOrientations: [Tile] {
        return [
            self,
            self.rotated,
            self.rotated.rotated,
            self.rotated.rotated.rotated,
            self.flippedHort,
            self.flippedVert,
            self.rotated.flippedHort,
            self.rotated.flippedVert,
        ]
    }
}

extension Tile: CustomDebugStringConvertible {
    var debugDescription: String {
        var output = "Tile \(id):\n"
        for line in data {
            output += line + "\n"
        }
        return output
    }
}

struct Day20: AdventOfCode {
    typealias InputData = [Tile]
    
    func parse(input: String) -> [Tile]? {
        let tileRegex = Regex(#"Tile (\d+):"#)
        var tileNumber = 0
        var tileLines: [String] = []
        var tiles: [Tile] = []
        for line in input.split(whereSeparator: \.isNewline) {
            if let capture = tileRegex.firstMatch(in: String(line))?.captures[0],
               let n = Int(capture) {
                if tileNumber != 0 {
                    tiles.append(Tile(id: tileNumber, data: tileLines))
                }
                tileLines = []
                tileNumber = n
            } else {
                tileLines.append(String(line))
            }
        }
        tiles.append(Tile(id: tileNumber, data: tileLines))
        return tiles
    }
    
    func getCorners(data: [Tile]) -> [Tile] {
        // Idea: the corners have a total of 4 borders (when normal and flipped) that do not appear in any other tile
        // (the other edges have 2 and the inner tiles have 0)
        // So we find all tiles with exactly 4 unique borders
        let uniqueBorders = data.map { (tile: Tile) -> (id: Int, borders: Set<String>) in
            let otherTiles = data.filter { $0.id != tile.id }
            let otherBorders = Set(otherTiles.map { otherTile in
                otherTile.allBorders
            }.reduce([], +))
            let myBorders = Set(tile.allBorders)
            return (id: tile.id, borders: Set(myBorders.filter { !otherBorders.contains($0) }))
        }
        let corners = uniqueBorders.filter { $0.borders.count == 4}
        let cornerIds = corners.map { $0.id }
        
        return data.filter { cornerIds.contains($0.id) }
    }
    
    func part1(data: [Tile]) {
        print(getCorners(data: data).map { $0.id}.reduce(1, *))
    }
    
    func part2(data: [Tile]) {
        // First, we need to build up the image:
        // Use one of the corners. Place it in the corner in any way possible. Then start adding tiles.
        let corners = getCorners(data: data)
        let topLeft = corners.first!
        
        // Get the four edges (including flipped) that are on the outside
        let allBorders = topLeft.allBorders
        let outsideBorders = Set(allBorders.filter { border in
            let otherTiles = data.filter { $0.id != topLeft.id }
            let otherBorders = Set(otherTiles.map { otherTile in
                otherTile.allBorders
            }.reduce([], +))
            return !otherBorders.contains(border)
        })
        
        let size = Int(Double(data.count).squareRoot())
        // All ways to place the top left tile.
        // Note that flipping both horizontal and vertical is the same as rotating twice, so we only have 8 ways to place it
        let topLeftOrientations = topLeft.allOrientations
        let validOrientations = topLeftOrientations.filter { tile in
            return outsideBorders.contains(tile.topBorder) && outsideBorders.contains(tile.leftBorder)
        }
        // We should have only two valid orientations
        assert(validOrientations.count == 2)
        // Pick any of the orientations
        let topLeftCorrect = validOrientations.last!
        
        var borderToTileMapping: [String: [Tile]] = [:]
        for tile in data {
            for border in tile.allBorders {
                borderToTileMapping[border] = (borderToTileMapping[border] ?? []) + [tile]
            }
        }
        
        // Now start placing the tiles
        let emptyTile = Tile(id: 0, data: [])
        let emptyRow = [Tile](repeating: emptyTile, count: size)
        var tileGrid = [[Tile]](repeating: emptyRow, count: size)
        tileGrid[0][0] = topLeftCorrect
        for row in 0..<size {
            for column in 0..<size {
                // We already placed the first tile
                if row == 0 && column == 0 {
                    continue
                }
                if column == 0 {
                    // If we are on the left edge, look at the tile above
                    let topTile = tileGrid[row - 1][column]
                    let topTileBottomBorder = topTile.bottomBorder
                    // Now find the tiles that have this border, there should be only one
                    let possibleTiles = borderToTileMapping[topTileBottomBorder]!.filter { $0.id != topTile.id }
                    assert(possibleTiles.count == 1)
                    let tileToPlace = possibleTiles.first!
                    
                    // Now check all orientations for this tile, only one should have the exact border. Again, there should be only one
                    let orientations = tileToPlace.allOrientations
                    let possibleOrientations = orientations.filter { $0.topBorder == topTileBottomBorder }
                    assert(possibleOrientations.count == 1)
                    tileGrid[row][column] = possibleOrientations.first!
                } else {
                    // If we are not on the left edge, look at the tile to the left
                    let leftTile = tileGrid[row][column - 1]
                    let leftTileRightBorder = leftTile.rightBorder
                    // Now find the tiles that have this border, there should be only one
                    let possibleTiles = borderToTileMapping[leftTileRightBorder]!.filter { $0.id != leftTile.id }
                    assert(possibleTiles.count == 1)
                    let tileToPlace = possibleTiles.first!
                    
                    // Now check all orientations for this tile, only one should have the exact border. Again, there should be only one
                    let orientations = tileToPlace.allOrientations
                    let possibleOrientations = orientations.filter { $0.leftBorder == leftTileRightBorder }
                    assert(possibleOrientations.count == 1)
                    tileGrid[row][column] = possibleOrientations.first!
                }
            }
        }
        
        // Now we know the whole grid. Build up the supertile from all tiles. We could have done that in the above loop but meh
        var lines: [String] = []
        for row in 0..<size {
            var linesForRow = [String](repeating: "", count: topLeft.data.count - 2)
            for column in 0..<size {
                let t = tileGrid[row][column]
                for d in 1..<topLeft.data.count - 1 {
                    let part = t.data[d]
                    linesForRow[d - 1] += part[part.index(after: part.startIndex)..<part.index(before:part.endIndex)]
                }
            }
            lines.append(contentsOf: linesForRow)
        }
        
        let superTile = Tile(id: 0, data: lines)
        
        let seaMonsterOffsets = [
            (0, 18),
            
            (1, 0),
            (1, 5),
            (1, 6),
            (1, 11),
            (1, 12),
            (1, 17),
            (1, 18),
            (1, 19),
            
            (2, 1),
            (2, 4),
            (2, 7),
            (2, 10),
            (2, 13),
            (2, 16),
        ]
        let seaMonsterRowHeight = seaMonsterOffsets.map { $0.0 }.max()!
        let seaMonsterColumnWidth = seaMonsterOffsets.map { $0.1 }.max()!
        
        let superOrientations = superTile.allOrientations
        for orientation in superOrientations {
            var seaMonsters: [(Int, Int)] = []
            var lines = orientation.data.map { [Character]($0) }
            for row in 0..<orientation.data.count - seaMonsterRowHeight {
                for column in 0..<orientation.data.count - seaMonsterColumnWidth {
                    // Check if we have a seamonster at this position
                    let possibleSeaMonsterData = seaMonsterOffsets.map {
                        lines[row + $0.0][column + $0.1]
                    }
                    let isSeaMonster = possibleSeaMonsterData
                        .filter { $0 == "#"}
                        .count == seaMonsterOffsets.count
                    if isSeaMonster {
                        seaMonsters.append((row, column))
                    }
                }
            }
            if seaMonsters.count > 0 {
                for seaMonster in seaMonsters {
                    // Replace the actual sea monsters
                    let seaMonsterSpots = seaMonsterOffsets.map {(seaMonster.0 + $0.0, seaMonster.1 + $0.1)}
                    for seaMonsterSpot in seaMonsterSpots {
                        lines[seaMonsterSpot.0][seaMonsterSpot.1] = "O"
                    }
                }
                
                print(
                    lines
                        .map { $0.filter { $0 == "#" }.count }
                        .reduce(0, +)
                )
            }
        }
    }
}

Day20().solve()
