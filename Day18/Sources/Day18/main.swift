import AoCShared

protocol Expr {
    func eval() -> Int
}

struct NumberExpr: Expr {
    let n: Int
    func eval() -> Int {
        return n
    }
}

struct PlusExpr: Expr {
    let l: Expr
    let r: Expr
    
    func eval() -> Int {
        return l.eval() + r.eval()
    }
}

struct TimesExpr: Expr {
    let l: Expr
    let r: Expr
    
    func eval() -> Int {
        return l.eval() * r.eval()
    }
}

enum Token: Equatable {
    case number(Int)
    case parenOpen
    case parenClose
    case plus
    case times
    
    static func toToken(input: String.SubSequence) -> Token? {
        switch input {
        case "+": return .plus
        case "*": return .times
        case "(": return .parenOpen
        case ")": return .parenClose
        default:
            if let n = Int(input) {
                return .number(n)
            }
            return nil
        }
    }
}

class Parser {
    let tokens: [Token]
    var idx = 0
    var next: Token? {
        return idx < tokens.count ? tokens[idx] : nil
    }
    
    init(tokens: [Token]) {
        self.tokens = tokens
    }
    
    func nextTokenIs(token: Token) -> Bool {
        return next == token
    }
    
    func process(token: Token) -> Bool {
        if nextTokenIs(token: token) {
            idx += 1
            return true
        }
        return false
    }
    
    func forceProcess(token: Token) {
        if !process(token: token) {
            preconditionFailure()
        }
    }
    
    func expr() -> Expr {
        var e = number()
        
        while next == .plus || next == .times {
            let op = next
            forceProcess(token: next!)
            let r = number()
            if op == .plus {
                e = PlusExpr(l: e, r: r)
            } else {
                e = TimesExpr(l: e, r: r)
            }
        }
        
        return e
    }
    
    func number() -> Expr {
        if process(token: .parenOpen) {
            let e = expr()
            forceProcess(token: .parenClose)
            return e
        }

        if case let .number(num) = next {
            forceProcess(token: .number(num))
            return NumberExpr(n: num)
        }

        preconditionFailure()
    }
    
    // Specific for part 2
    func times() -> Expr {
        var e = plus()
        while next == .times {
            forceProcess(token: next!)
            let r = plus()
            e = TimesExpr(l: e, r: r)
        }
        
        return e
    }
    func plus() -> Expr {
        var e = number2()
        while next == .plus {
            forceProcess(token: next!)
            let r = number2()
            e = PlusExpr(l: e, r: r)
        }
        
        return e
    }
    
    func number2() -> Expr {
        if process(token: .parenOpen) {
            let e = times()
            forceProcess(token: .parenClose)
            return e
        }

        if case let .number(num) = next {
            forceProcess(token: .number(num))
            return NumberExpr(n: num)
        }

        preconditionFailure()
    }
}

struct Day18: AdventOfCode {
    typealias InputData = [[Token]]
    
    func parse(input: String) -> [[Token]]? {
        input
            .split(whereSeparator: \.isNewline)
            .compactMap { line in
                line
                    .replacingOccurrences(of: "(", with: " ( ")
                    .replacingOccurrences(of: ")", with: " ) ")
                    .split(separator: " ")
                    .compactMap { Token.toToken(input: $0) }
            }
    }
    
    func part1(data: [[Token]]) {
        print(data
                .map { Parser(tokens: $0).expr() }
                .map { $0.eval() }.reduce(0, +))
    }
    
    func part2(data: [[Token]]) {
        print(data
                .map { Parser(tokens: $0).times() }
                .map { $0.eval() }.reduce(0, +))
    }
}

Day18().solve()
