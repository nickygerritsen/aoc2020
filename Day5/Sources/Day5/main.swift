import AoCShared

struct Day5: AdventOfCode {
    typealias InputData = Set<Int>
    
    func parse(input: String) -> Set<Int>? {
        return Set<Int>(input
            .split(whereSeparator: \.isNewline)
            .compactMap { String($0) }
            .map { $0.replacingOccurrences(of: "F", with: "0") }
            .map { $0.replacingOccurrences(of: "B", with: "1") }
            .map { $0.replacingOccurrences(of: "L", with: "0") }
            .map { $0.replacingOccurrences(of: "R", with: "1") }
            .compactMap { Int($0, radix: 2) })
    }
    
    func part1(data: Set<Int>) {
        if let m = data.max() {
            print(m)
        }
    }
    
    func part2(data: Set<Int>) {
        if let m = data.max() {
            for i in 1..<m {
                if !data.contains(i) && data.contains(i - 1) && data.contains(i + 1) {
                    print(i)
                }
            }
        }
    }
}


Day5().solve()
