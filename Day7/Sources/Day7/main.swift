import AoCShared
import Regex

struct Day7: AdventOfCode {
    typealias InputData = [String: [String: Int]]
    
    func parse(input: String) -> [String: [String: Int]]? {
        var data: [String: [String: Int]] = [:]
        for line in input.split(whereSeparator: \.isNewline).map({ String($0) }) {
            let r = Regex(#"^([a-z ]+) bags contain ([a-z0-9, ]+)\.$"#)
            let br = Regex(#"^(\d+) (.*)$"#)
            
            if let match = r.firstMatch(in: String(line)) {
                if let nodeName = match.captures[0], let otherBags = match.captures[1] {
                    if otherBags == "no other bags" {
                        data[nodeName] = [:]
                    } else {
                        var connected: [String: Int] = [:]
                        for bag in otherBags
                            .split(separator: ",")
                            .map({ $0.trimmingCharacters(in: .whitespacesAndNewlines) })
                            .map({ $0.replacingOccurrences(of: "bags", with: "bag")})
                            .map({ $0.replacingOccurrences(of: " bag", with: "")}){
                            if let m = br.firstMatch(in: bag) {
                                if let n = m.captures[0], let bn = m.captures[1], let nn = Int(n) {
                                    connected[bn] = nn
                                }
                            }
                        }
                        data[nodeName] = connected
                    }
                }
            }
        }
        
        return data
    }

    func canHold(data: [String: [String: Int]], name: String, toHold: String) -> Bool {
        if let holds = data[name] {
            if holds[toHold] != nil {
                return true
            }
            for k in holds.keys {
                if canHold(data: data, name: k, toHold: toHold) {
                    return true
                }
            }
            
            return false
        } else {
            return false
        }
    }
    
    func part1(data: [String: [String: Int]]) {
        print(
            data
                .keys
                .filter { canHold(data: data, name: $0, toHold: "shiny gold") }
                .count
        )
    }
    
    func totalCount(data: [String: [String: Int]], name: String, amount: Int) -> Int {
        if let s = data[name] {
            return amount * (1 + s.reduce(0) { $0 + totalCount(data: data, name: $1.key, amount: $1.value) })
        } else {
            return 1
        }
    }
    
    func part2(data: [String: [String: Int]]) {
        if let s = data["shiny gold"] {
            print(s.reduce(0) { $0 + totalCount(data: data, name: $1.key, amount: $1.value) })
        }
    }
}

Day7().solve()
