import AoCShared

struct Day9: AdventOfCode {
    
    let preamble = 25
    typealias InputData = [Int]
    func parse(input: String) -> [Int]? {
        input.split(whereSeparator: \.isNewline).compactMap { Int(String($0)) }
    }
    
    func containsSum(part: ArraySlice<Int>, n: Int) -> Bool {
        for i in part.startIndex..<part.endIndex {
            for j in part.startIndex..<part.endIndex {
                if part[i] != part[j] && part[i] + part[j] == n {
                    return true
                }
            }
        }
        return false
    }
    
    func firstInvalidNumber(data: [Int]) -> Int {
        var b = 0
        for c in preamble..<data.count {
            if !containsSum(part: data[b..<c], n: data[c]) {
                return data[c]
            }
            b += 1
        }
        return -1
    }
    
    func part1(data: [Int]) {
        print(firstInvalidNumber(data: data))
    }
    
    func part2(data: [Int]) {
        let invalidNumber = firstInvalidNumber(data: data)
        
        for i in data.startIndex..<data.endIndex {
            var sum = data[i]
            for j in i + 1..<data.endIndex {
                sum += data[j]
                if sum == invalidNumber {
                    let minValue = data[i...j].reduce(Int.max) { min($0, $1) }
                    let maxValue = data[i...j].reduce(0) { max($0, $1) }
                    print(minValue + maxValue)
                }
            }
        }
    }
}

Day9().solve()
