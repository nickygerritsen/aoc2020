import AoCShared
import Regex
import Foundation

let maskRegex = Regex(#"^mask = (.*)$"#)
let memRegex = Regex(#"^mem\[(\d+)\] = (\d+)$"#)

let length = 36

enum Command {
    case mask(mask: [Character])
    case setMem(address: Int, value: Int)
    
    static func parse(line: String) -> Command? {
        if let captures = maskRegex.firstMatch(in: line)?.captures,
           let mask = captures[0] {
            return .mask(mask: [Character](mask))
        } else if let captures = memRegex.firstMatch(in: line)?.captures,
                  let address = captures[0],
                  let value = captures[1],
                  let addressValue = Int(address),
                  let valueValue = Int(value) {
            return .setMem(address: addressValue, value: valueValue)
        }
        
        return nil
    }
}

func masked(number: Int, mask: [Character]) -> Int? {
    let binary = [Character](String(number, radix: 2))
    let fullBinary = [Character](String(repeating: "0", count: length - binary.count) + binary)
    let finalNumber = (0..<length).map { (index: Int) -> Character in
        let b = fullBinary[index]
        let m = mask[index]
        if m == "X" {
            return b
        } else {
            return m
        }
    }
    return Int(String(finalNumber), radix: 2)
}

struct Day14: AdventOfCode {
    typealias InputData = [Command]
    
    func parse(input: String) -> [Command]? {
        input
            .split(whereSeparator: \.isNewline)
            .compactMap { Command.parse(line: String($0)) }
    }
    
    func part1(data: [Command]) {
        var memory: [Int: Int] = [:]
        var currentMask = [Character](repeating: "X", count: length)
        for command in data {
            switch command {
            case .mask(mask: let mask):
                currentMask = mask
            case .setMem(address: let address, value: let value):
                guard let v = masked(number: value, mask: currentMask) else {
                    print("PANIC")
                    return
                }
                memory[address] = v
            }
        }
        
        let sum = memory.reduce(0) { $0 + $1.value }
        print(sum)
    }
    
    func part2(data: [Command]) {
        var memory: [Int: Int] = [:]
        var currentMask = [Character](repeating: "X", count: length)
        for command in data {
            switch command {
            case .mask(mask: let mask):
                currentMask = mask
            case .setMem(address: let address, value: let value):
                let binaryAddress = [Character](String(address, radix: 2))
                let fullBinaryAddress = [Character](String(repeating: "0", count: length - binaryAddress.count) + binaryAddress)
                let numX = currentMask.filter { $0 == "X" }.count
                let xIndices = currentMask
                    .enumerated()
                    .filter({ $0.element == "X" }).map({ $0.offset })
                for i in 0..<Int(pow(Double(2), Double(numX))) {
                    let binaryI = [Character](String(i, radix: 2))
                    let fullBinaryI = [Character](repeating: "0", count: numX - binaryI.count) + binaryI
                    var decodedAddress = fullBinaryAddress
                    for p in 0..<numX {
                        let pos = xIndices[p]
                        let v = fullBinaryI[p]
                        decodedAddress[pos] = v
                    }
                    for (idx, val) in currentMask.enumerated() {
                        if val == "1" {
                            decodedAddress[idx] = "1"
                        }
                    }
                    guard let address = Int(String(decodedAddress), radix: 2) else {
                        print("PANIC")
                        return
                    }
                    memory[address] = value
                }
            }
        }
        
        let sum = memory.reduce(0) { $0 + $1.value }
        print(sum)
    }
}

Day14().solve()
