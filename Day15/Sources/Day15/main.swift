import AoCShared

struct Day15: AdventOfCode {
    typealias InputData = [Int]
    
    func parse(input: String) -> [Int]? {
        input
            .trimmingCharacters(in: .whitespacesAndNewlines)
            .split(separator: ",")
            .compactMap { Int(String($0)) }
    }
    
    func solve(data: [Int], n: Int) -> Int {
        var lastSpoken = Array<Int>(repeating: -1, count: n + 1)
        var lastSpokenBefore = Array<Int>(repeating: -1, count: n + 1)
        for (i, d) in data.enumerated() {
            lastSpoken[d] = i
        }
        var lastNumber = data.last!
        for i in data.count..<n {
            if lastSpoken[lastNumber] != -1 && lastSpokenBefore[lastNumber] != -1 {
                lastNumber = lastSpoken[lastNumber] - lastSpokenBefore[lastNumber]
            } else {
                lastNumber = 0
            }
            if lastSpoken[lastNumber] != -1 {
                lastSpokenBefore[lastNumber] = lastSpoken[lastNumber]
            }
            lastSpoken[lastNumber] = i
        }
        
        return lastNumber
    }
    
    func part1(data: [Int]) {
        print(solve(data: data, n: 2020))
    }
    
    func part2(data: [Int]) {
        print(solve(data: data, n: 30000000))
    }
}

Day15().solve()
