import AoCShared

enum Move {
    case north(amount: Int)
    case south(amount: Int)
    case east(amount: Int)
    case west(amount: Int)
    case left(amount: Int)
    case right(amount: Int)
    case forward(amount: Int)
    
    static func parse(line: String.SubSequence) -> Move? {
        if let n = Int(String(line[line.index(after: line.startIndex)...])) {
            let c = line[line.startIndex]
            switch c {
            case "N":
                return .north(amount: n)
            case "S":
                return .south(amount: n)
            case "E":
                return .east(amount: n)
            case "W":
                return .west(amount: n)
            case "L":
                return .left(amount: n)
            case "R":
                return .right(amount: n)
            case "F":
                return .forward(amount: n)
            default:
                return nil
            }
        }
        
        return nil
    }
}

enum Direction: Int {
    case north = 0
    case east = 1
    case south = 2
    case west = 3
    
    func forwardDirection() -> (Int, Int) {
        switch self {
        case .north:
            return (0, 1)
        case .east:
            return (1, 0)
        case .south:
            return (0, -1)
        case .west:
            return (-1, 0)
        }
    }
}

struct Ship {
    var x = 0
    var y = 0
    var direction = Direction.east
    
    mutating func perform(move: Move) {
        switch move {
        case .north(amount: let amount):
            y += amount
        case .south(amount: let amount):
            y -= amount
        case .east(amount: let amount):
            x += amount
        case .west(amount: let amount):
            x -= amount
        case .left(amount: let amount):
            let steps = amount / 90
            direction = Direction.init(rawValue: (4 + direction.rawValue - steps) % 4)!
        case .right(amount: let amount):
            let steps = amount / 90
            direction = Direction.init(rawValue: (direction.rawValue + steps) % 4)!
        case .forward(amount: let amount):
            let toAdd = direction.forwardDirection()
            x += toAdd.0 * amount
            y += toAdd.1 * amount
        }
    }
}

struct ShipWithWaypoint {
    var x = 0
    var y = 0
    var wx = 10
    var wy = 1
    var direction = Direction.east
    
    mutating func perform(move: Move) {
        switch move {
        case .north(amount: let amount):
            wy += amount
        case .south(amount: let amount):
            wy -= amount
        case .east(amount: let amount):
            wx += amount
        case .west(amount: let amount):
            wx -= amount
        case .left(amount: let amount):
            let steps = 4 - ((amount / 90) % 4)
            rotateWaypoint(steps: steps)
        case .right(amount: let amount):
            let steps = (amount / 90) % 4
            rotateWaypoint(steps: steps)
        case .forward(amount: let amount):
            x += amount * wx
            y += amount * wy
        }
    }
    
    mutating func rotateWaypoint(steps: Int) {
        switch steps {
        case 1:
            (wx, wy) = (wy, -wx)
        case 2:
            (wx, wy) = (-wx, -wy)
        case 3:
            (wx, wy) = (-wy, wx)
        default:
            return
        }
    }
}

extension Move: CustomDebugStringConvertible {
    var debugDescription: String {
        switch self {
        case .north(amount: let amount):
            return "N\(amount)"
        case .south(amount: let amount):
            return "S\(amount)"
        case .east(amount: let amount):
            return "E\(amount)"
        case .west(amount: let amount):
            return "W\(amount)"
        case .left(amount: let amount):
            return "L\(amount)"
        case .right(amount: let amount):
            return "R\(amount)"
        case .forward(amount: let amount):
            return "F\(amount)"
        }
    }
}

struct Day12: AdventOfCode {
    typealias InputData = [Move]
    func parse(input: String) -> [Move]? {
        input.split(whereSeparator: \.isNewline).compactMap { Move.parse(line: $0) }
    }
    
    func part1(data: [Move]) {
        var ship = Ship()
        for move in data {
            ship.perform(move: move)
        }
        print(abs(ship.x) + abs(ship.y))
    }
    
    func part2(data: [Move]) {
        var ship = ShipWithWaypoint()
        for move in data {
            ship.perform(move: move)
        }
        print(abs(ship.x) + abs(ship.y))
    }
}

Day12().solve()
