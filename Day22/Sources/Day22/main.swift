import AoCShared

struct Player: Hashable {
    var cards: Array<Int>
}

struct PlayerTuple: Hashable {
    var player1: Player
    var player2: Player
}

extension PlayerTuple: CustomDebugStringConvertible {
    var debugDescription: String {
        let p1Cards = player1.cards.map { String($0) }.joined(separator: ", ")
        let p2Cards = player2.cards.map { String($0) }.joined(separator: ", ")
        return "Player 1's deck: \(p1Cards)\nPlayer 2's deck: \(p2Cards)"
    }
}

struct Game {
    var player1: Player
    var player2: Player
    var canPlay: Bool {
        return !player1.cards.isEmpty && !player2.cards.isEmpty
    }
    
    mutating func playRound() {
        let p1 = player1.cards.first!
        let p2 = player2.cards.first!
        
        if p1 > p2 {
            player1.cards = player1.cards[1..<player1.cards.count] + [p1, p2]
            player2.cards = Array(player2.cards[1..<player2.cards.count])
        } else {
            player1.cards = Array(player1.cards[1..<player1.cards.count])
            player2.cards = player2.cards[1..<player2.cards.count] + [p2, p1]
        }
    }
}

struct RecursiveGame {
    var players: PlayerTuple
    var canPlay: Bool {
        return !players.player1.cards.isEmpty && !players.player2.cards.isEmpty
    }
    
    // Returns whether player 1 wins
    mutating func playGame() -> Bool {
        var previousRound: Set<PlayerTuple> = []
        
        var round = 1
        while true {
            if previousRound.contains(players) {
                return true
            } else {
                previousRound.insert(players)
                let p1 = players.player1.cards.first!
                let p2 = players.player2.cards.first!
                players.player1.cards = Array(players.player1.cards[1..<players.player1.cards.count])
                players.player2.cards = Array(players.player2.cards[1..<players.player2.cards.count])
                
                if players.player1.cards.count >= p1 && players.player2.cards.count >= p2 {
                    // Play a recursive game
                    let p1Sub = Player(cards: Array(players.player1.cards[0..<p1]))
                    let p2Sub = Player(cards: Array(players.player2.cards[0..<p2]))
                    var subGame = RecursiveGame(players: PlayerTuple(player1: p1Sub, player2: p2Sub))
                    let winner = subGame.playGame()
                    if winner {
                        // Player 1 wins, so he gets both cards
                        players.player1.cards += [p1, p2]
                    } else {
                        // Player 1 wins, so he gets both cards
                        players.player2.cards += [p2, p1]
                    }
                } else {
                    if p1 > p2 {
                        // Player 1 wins, so he gets both cards
                        players.player1.cards += [p1, p2]
                    } else {
                        // Player 1 wins, so he gets both cards
                        players.player2.cards += [p2, p1]
                    }
                }
            }
            
            if players.player1.cards.isEmpty {
                return false
            } else if players.player2.cards.isEmpty {
                return true
            }
            round += 1
        }
    }
}

struct Day22: AdventOfCode {
    typealias InputData = Game
    
    func parse(input: String) -> Game? {
        var player1: Player = Player(cards: [])
        var player2: Player = Player(cards: [])
        var forPlayer1 = true
        for line in input.split(whereSeparator: \.isNewline) {
            if line == "Player 1:" {
                // Ignore
            } else if line == "Player 2:" {
                forPlayer1 = false
            } else if let n = Int(line) {
                if forPlayer1 {
                    player1.cards.append(n)
                } else {
                    player2.cards.append(n)
                }
            }
        }
        
        return Game(player1: player1, player2: player2)
    }
    
    func part1(data: Game) {
        var game = data
        while game.canPlay {
            game.playRound()
        }

        let winner = game.player1.cards.isEmpty ? game.player2 : game.player2
        print(winner.cards.reversed().enumerated().map { ($0.offset + 1) * $0.element }.reduce(0, +))
    }
    
    func part2(data: Game) {
        var game = RecursiveGame(players: PlayerTuple(player1: data.player1, player2: data.player2))
        let winner = game.playGame() ? game.players.player1 : game.players.player2
        print(winner.cards.reversed().enumerated().map { ($0.offset + 1) * $0.element }.reduce(0, +))
    }
}

Day22().solve()
