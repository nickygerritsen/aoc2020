import AoCShared

struct Day1: AdventOfCode {
    typealias InputData = [Int]
    let expected = 2020

    func parse(input: String) -> [Int]? {
        input.split(whereSeparator: \.isNewline).compactMap {
            Int($0)
        }
    }

    func part1(data: [Int]) {
        for i in data.startIndex..<data.endIndex {
            for j in i + 1..<data.endIndex {
                if data[i] + data[j] == expected {
                    print(data[i] * data[j])
                }
            }
        }
    }

    func part2(data: [Int]) {
        for i in data.startIndex..<data.endIndex {
            for j in i + 1..<data.endIndex {
                for k in j + 1..<data.endIndex {
                    if data[i] + data[j] + data[k] == expected {
                        print(data[i] * data[j] * data[k])
                    }
                }
            }
        }
    }
}

Day1().solve()