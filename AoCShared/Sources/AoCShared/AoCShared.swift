import Foundation

public protocol AdventOfCode {
    associatedtype InputData
    func parse(input: String) -> InputData?
    func part1(data: InputData)
    func part2(data: InputData)
}

extension AdventOfCode {
    public func solve(printData: Bool = false) {
        // Try the current directory and three directories up
        let components = [
            ["input.txt"],
            ["..", "..", "..", "input.txt"],
        ]

        guard let input = components.compactMap({ getContents(components: $0) }).first else {
            print("File not found or can not be read")
            return
        }

        guard let data = parse(input: input) else {
            print("Can not parse data")
            return
        }

        if printData {
            print("Parsed input data:")
            print(data)
        }

        part1(data: data)
        part2(data: data)
    }

    func getContents(components: [String]) -> String? {
        guard let url = NSURL.fileURL(withPathComponents: components) else {
            return nil
        }

        guard let input = try? String(contentsOf: url) else {
            return nil
        }

        return input
    }
}
