import AoCShared

struct PasswordPart {
    let lower: Int
    let higher: Int
    let letter: Character
    let password: [Character]

    static func parse(item: String) -> PasswordPart?
    {
        let parts = item.split(separator: " ")
        let bounds = parts[0].split(separator: "-")
        guard let lower = Int(bounds[0]) else {
            return nil
        }
        guard let higher = Int(bounds[1]) else {
            return nil
        }
        guard let letter = parts[1].first else {
            return nil
        }

        return PasswordPart(lower: lower, higher: higher, letter: letter, password: [Character](parts[2]))
    }
}

struct Day2: AdventOfCode {
    typealias InputData = [PasswordPart]
    func parse(input: String) -> [PasswordPart]? {
        input.split(whereSeparator: \.isNewline).compactMap {
            PasswordPart.parse(item: String($0))
        }
    }

    func part1(data: [PasswordPart]) {
        let answer = data.filter { part in
            let count = part.password.filter { $0 == part.letter }.count
            return count >= part.lower && count <= part.higher
        }.count
        print(answer)
    }

    func part2(data: [PasswordPart]) {
        let answer = data.filter { part in
            let firstOk = part.password[part.lower - 1] == part.letter
            let secondOk = part.password[part.higher - 1] == part.letter
            return firstOk != secondOk
        }.count
        print(answer)
    }
}

Day2().solve()