import AoCShared

enum Direction: String, CaseIterable {
    case e = "e"
    case w = "w"
    case se = "se"
    case sw = "sw"
    case ne = "ne"
    case nw = "nw"
    
    var move: Position {
        switch self {
        case .e:
            return Position(x: 2, y: 0)
        case .w:
            return Position(x: -2, y: 0)
        case .se:
            return Position(x: 1, y: -1)
        case .sw:
            return Position(x: -1, y: -1)
        case .ne:
            return Position(x: 1, y: 1)
        case .nw:
            return Position(x: -1, y: 1)
        }
    }
}

struct Instruction {
    let directions: [Direction]
}

struct Position: Hashable {
    let x: Int
    let y: Int
    
    static func +(lhs: Position, rhs: Position) -> Position {
        return Position(x: lhs.x + rhs.x, y: lhs.y + rhs.y)
    }
}

struct Day24: AdventOfCode {
    typealias InputData = [Instruction]
    
    func parse(input: String) -> [Instruction]? {
        return input.split(whereSeparator: \.isNewline).map { line in
            let l = [Character](line)
            var directions: [Direction] = []
            
            var idx = 0
            while idx < l.count {
                let c = l[idx]
                let s: String
                if c == "n" || c == "s" {
                    s = String(c) + String(l[idx + 1])
                    idx += 1
                } else {
                    s = String(c)
                }
                
                if let d = Direction(rawValue: s) {
                    directions.append(d)
                }
                
                idx += 1
            }
            
            return Instruction(directions: directions)
        }
    }
    
    func initialFlipping(data: [Instruction]) -> [Position: Bool] {
        var tiles: [Position: Bool] = [:]
        data.forEach { instruction in
            let p = instruction.directions.map { $0.move }.reduce(Position(x: 0, y: 0), +)
            tiles[p] = !(tiles[p] ?? false)
        }
        
        return tiles
    }
    
    func part1(data: [Instruction]) {
        print(initialFlipping(data: data).filter { $0.value }.count)
    }
    
    func part2(data: [Instruction]) {
        let initial = initialFlipping(data: data)
        let directions = Direction.allCases.map { $0.move }
        
        var current = initial
        for _ in 0..<100 {
            var new = current
            
            let blackTiles = current.filter { $0.value }.keys
            var toConsider = Set(blackTiles)
            for tile in blackTiles {
                directions.forEach { d in
                    toConsider.insert(tile + d)
                }
            }
            
            toConsider.forEach { tile in
                let tileIsBlack = current[tile] ?? false
                let numBlackNeighbors = directions
                    .map { $0 + tile }
                    .map { current[$0] ?? false }
                    .filter { $0 }
                    .count
                
                if tileIsBlack && (numBlackNeighbors == 0 || numBlackNeighbors > 2) {
                    new[tile] = false
                }
                if !tileIsBlack && numBlackNeighbors == 2 {
                    new[tile] = true
                }
            }
            
            current = new
        }
        
        print(current.filter { $0.value }.count)
    }
}

Day24().solve()
