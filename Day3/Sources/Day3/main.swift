import AoCShared

enum Item: Character {
    case tree = "#"
    case open = "."
}

struct Day3: AdventOfCode {
    typealias InputData = [[Item]]

    func parse(input: String) -> [[Item]]? {
        input.split(whereSeparator: \.isNewline).map { line in
            line.compactMap { char in
                Item(rawValue: char)
            }
        }
    }

    func numTrees(data: [[Item]], dx: Int, dy: Int) -> Int {
        var x = 0
        var y = 0
        var cnt = 0
        while y < data.count {
            if data[y][x] == .tree {
                cnt += 1
            }
            x = (x + dx) % data[0].count
            y += dy
        }
        return cnt
    }

    func part1(data: [[Item]]) {
        print(numTrees(data: data, dx: 3, dy: 1))
    }

    func part2(data: [[Item]]) {
        print([
            numTrees(data: data, dx: 1, dy: 1),
            numTrees(data: data, dx: 3, dy: 1),
            numTrees(data: data, dx: 5, dy: 1),
            numTrees(data: data, dx: 7, dy: 1),
            numTrees(data: data, dx: 1, dy: 2),
        ].reduce(1, { $0 * $1 }))
    }
}

Day3().solve()