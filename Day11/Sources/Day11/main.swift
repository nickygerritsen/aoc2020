import AoCShared

enum Position: String {
    case floor = "."
    case seat = "L"
    case occupiedSeat = "#"
}

struct Point: Hashable {
    let r: Int
    let c: Int
}

struct FloorPlan {
    var rows: [[Position]]
    
    init(rows: [[Position]]) {
        // Add a row above and below and a column to the left and right
        self.rows =
            [[Position](repeating: .floor, count: rows[0].count + 2)] +
            rows.map { [Position.floor] + $0 + [Position.floor] } +
            [[Position](repeating: .floor, count: rows[0].count + 2)]
    }
    
    mutating func stepPart1() {
        var newRows = rows
        for r in 1..<rows.count - 1 {
            for c in 1..<rows[r].count - 1 {
                let adjacentSeets = [
                    rows[r-1][c-1],
                    rows[r  ][c-1],
                    rows[r+1][c-1],
                    rows[r-1][c  ],
                    rows[r+1][c  ],
                    rows[r-1][c+1],
                    rows[r  ][c+1],
                    rows[r+1][c+1],
                ]
                if rows[r][c] == .seat {
                    if !adjacentSeets.contains(.occupiedSeat) {
                        newRows[r][c] = .occupiedSeat
                    }
                } else if rows[r][c] == .occupiedSeat {
                    let occupiedCount = adjacentSeets.filter { $0 == .occupiedSeat }.count
                    if occupiedCount >= 4 {
                        newRows[r][c] = .seat
                    }
                }
            }
        }
        
        rows = newRows
    }
    
    func outside(r: Int, c: Int, countr: Int, countc: Int) -> Bool {
        if r <= 0 || c <= 0 {
            return true
        }
        if r >= countr || c >= countc {
            return true
        }
        return false
    }
    
    // O(N^2)
    mutating func stepPart2() {
        var newRows = rows
        let dd = [
            (r: -1, c: -1),
            (r:  0, c: -1),
            (r:  1, c: -1),
            (r: -1, c:  0),
            (r:  1, c:  0),
            (r: -1, c:  1),
            (r:  0, c:  1),
            (r:  1, c:  1),
        ]
        
        // First, calculate the position of the adjacent seats
        var adjacentSeats: [Point: [Point]] = [:]
        for r in 1..<rows.count - 1 {
            for c in 1..<rows[r].count - 1 {
                var adj: [Point] = []
                for d in dd {
                    var cnt = 1
                    while !outside(r: r + d.r * cnt, c: c + d.c * cnt, countr: rows.count, countc: rows[r].count) {
                        if rows[r + d.r * cnt][c + d.c * cnt] != .floor {
                            adj.append(Point(r: r + d.r * cnt, c: c + d.c * cnt))
                            break
                        }
                        cnt += 1
                    }
                }
                adjacentSeats[Point(r: r, c: c)] = adj
            }
        }
        
        for r in 1..<rows.count - 1 {
            for c in 1..<rows[r].count - 1 {
                if let adjPoints = adjacentSeats[Point(r: r, c: c)] {
                    let adjacentSeets = adjPoints.map { rows[$0.r][$0.c] }
                    
                    if rows[r][c] == .seat {
                        if !adjacentSeets.contains(.occupiedSeat) {
                            newRows[r][c] = .occupiedSeat
                        }
                    } else if rows[r][c] == .occupiedSeat {
                        let occupiedCount = adjacentSeets.filter { $0 == .occupiedSeat }.count
                        if occupiedCount >= 5 {
                            newRows[r][c] = .seat
                        }
                    }
                }
            }
        }
        
        rows = newRows
    }
}

extension FloorPlan: CustomDebugStringConvertible {
    var debugDescription: String {
        var s = ""
        for row in rows {
            for col in row {
                s += col.rawValue
            }
            s += "\n"
        }
        
        return s
    }
}

struct Day11: AdventOfCode {
    typealias InputData = FloorPlan
    
    func parse(input: String) -> FloorPlan? {
        FloorPlan(rows: input.split(whereSeparator: \.isNewline).map { line in
            line.compactMap { Position(rawValue: String($0)) }
        })
    }
    
    func part1(data: FloorPlan) {
        var floor = data
        var oldRows = floor.rows
        floor.stepPart1()
        while oldRows != floor.rows {
            oldRows = floor.rows
            floor.stepPart1()
        }
        
        print(
            floor.rows.reduce(0) { (acc, row) in
                acc + row.filter { $0 == .occupiedSeat }.count
            }
        )
    }
    
    func part2(data: FloorPlan) {
        var floor = data
        var oldRows = floor.rows
        floor.stepPart2()
        while oldRows != floor.rows {
            oldRows = floor.rows
            floor.stepPart2()
        }
        
        print(
            floor.rows.reduce(0) { (acc, row) in
                acc + row.filter { $0 == .occupiedSeat }.count
            }
        )
    }
}

Day11().solve()
