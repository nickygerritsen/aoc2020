import AoCShared
import Regex

let charRegex = Regex(#"^"\w+"$"#)

enum Rule {
    case char(Character)
    case options([[Int]])
    case recursion(([Character], Int) -> Int?)
    
    static func make(s: String) -> Rule {
        if charRegex.matches(s) {
            let char = s[s.index(after: s.startIndex)]
            return .char(char)
        } else {
            let parts = s.split(separator: "|").map { part in
                return part
                    .split(separator: " ")
                    .compactMap { Int($0) }
            }
            return .options(parts)
        }
    }
    
    // Check the string from the given input. Will return where the match ends or nil of no match
    func check(rules: [Int: Rule], input: [Character], index: Int = 0) -> Int? {
        if index >= input.count {
            return nil
        }
        switch self {
        case let .char(c):
            if input[index] == c {
                return index + 1
            } else {
                return nil
            }
        case let .recursion(f):
            return f(input, index)
        case let .options(options):
            for option in options {
                var match = true
                var indexCopy = index
                for rule in option {
                    if let subRule = rules[rule],
                       let newIndex = subRule.check(rules: rules, input: input, index: indexCopy) {
                        indexCopy = newIndex
                    } else {
                        match = false
                        break
                    }
                }
                
                if match {
                    return indexCopy
                }
            }
            
            return nil
        }
    }
}

struct Data {
    let rules: [Int: Rule]
    let messages: [String]
}

struct Day19: AdventOfCode {
    typealias InputData = Data
    
    func parse(input: String) -> Data? {
        let lines = input.split(whereSeparator: \.isNewline)
        let rules = lines
            .filter { $0.contains(":") }
            .map { String($0) }
        let messages = lines
            .filter { !$0.contains(":") }
            .map { String($0) }
        var rulesDict: [Int: Rule] = [:]
        for r in rules {
            let s = r.split(separator: ":")
            if let n = Int(s[0]) {
                rulesDict[n] = Rule.make(s: String(s[1]).trimmingCharacters(in: .whitespaces))
            }
        }
        return Data(rules: rulesDict, messages: messages)
    }
    
    func part1(data: Data) {
        if let rule = data.rules[0] {
            print(data.messages.filter { rule.check(rules: data.rules, input: [Character]($0) ) == $0.count }.count)
        }
    }
    
    func part2(data: Data) {
        var newRules = data.rules
        newRules[0] = .recursion({ (input, index) -> Int? in
            // The rule is basically: do N times 42 and then M times 31, with
            // * N >= 2
            // * M >= 1
            // * M < N
            var index: Int? = index
            
            guard let r42 = newRules[42], let r31 = newRules[31] else {
                return nil
            }
            
            
            // Keep track of how often we do rule 42
            var matches42 = 0
            while true {
                let indexSub = r42.check(rules: newRules, input: input, index: index!)
                if indexSub == nil {
                    break
                } else {
                    index = indexSub
                    matches42 += 1
                }
            }
            
            // Now keep track of how often we do rule 31
            var matches31 = 0
            while true {
                let indexSub = r31.check(rules: newRules, input: input, index: index!)
                if indexSub == nil {
                    break
                } else {
                    index = indexSub
                    matches31 += 1
                }
            }
            
            // Now check if the values are OK
            if matches42 < 2 || matches31 < 1 || matches31 >= matches42 {
                return nil
            } else {
                return index
            }
        })
        if let rule = newRules[0] {
            print(data.messages.filter { rule.check(rules: data.rules, input: [Character]($0) ) == $0.count }.count)
        }
    }
}

Day19().solve()
