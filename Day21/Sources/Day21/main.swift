import AoCShared
import Regex

struct Food {
    let ingredients: Set<String>
    let allergens: Set<String>
}

struct Day21: AdventOfCode {
    typealias InputData = [Food]
    func parse(input: String) -> [Food]? {
        let regex = Regex(#"^(.*) \(contains (.*)\)$"#)
        return input
            .split(whereSeparator: \.isNewline)
            .compactMap { line in
                let l = String(line)
                if let captures = regex.firstMatch(in: l)?.captures {
                    if let ingredients = captures[0]?.split(separator: " ").map({ String($0)}),
                       let allergens = captures[1]?.split(separator: " ").compactMap({ String($0).replacingOccurrences(of: ",", with: "") }) {
                        return Food(ingredients: Set(ingredients), allergens: Set(allergens))
                    }
                }
                
                return nil
            }
    }
    
    func cantContainAllergens(data: [Food]) -> [String] {
        var allergenMap: [String: Set<String>] = [:]
        for allergen in Set(data.map({ $0.allergens }).reduce([], +)) {
            var s: Set<String> = []
            let foodThatContainsAllergen = data.filter { $0.allergens.contains(allergen) }
            for food in foodThatContainsAllergen {
                if s.isEmpty {
                    s = food.ingredients
                } else {
                    s.formIntersection(food.ingredients)
                }
            }
            allergenMap[allergen] = s
        }
        
        let allMatchedIngredients = Set(allergenMap
            .map { $0.value }
            .reduce([], +))
        
        return data.map { food in
            food.ingredients.filter { !allMatchedIngredients.contains($0) }
        }.reduce([], +)
    }
    
    func part1(data: [Food]) {
        print(cantContainAllergens(data: data).count)
    }
    
    func part2(data: [Food]) {
        let cantContainAllergenSet = Set(cantContainAllergens(data: data))
        var canContainMap: [String: Set<String>] = [:]
        for food in data {
            for allergen in food.allergens {
                let ingredientsLeft = food.ingredients.filter { !cantContainAllergenSet.contains($0 )}
                if let m = canContainMap[allergen] {
                    canContainMap[allergen] = ingredientsLeft.intersection(m)
                } else {
                    canContainMap[allergen] = ingredientsLeft
                }
            }
        }
        
        var assignment: [String: String] = [:]
        while !canContainMap.isEmpty {
            let singleAssignment = canContainMap.filter { $0.value.count == 1}.first!
            assignment[singleAssignment.key] = singleAssignment.value.first!
            canContainMap = canContainMap.mapValues { set in
                set.filter { $0 != singleAssignment.value.first! }
            }
            canContainMap = canContainMap.filter { !$0.value.isEmpty }
        }
        
        print(
            assignment
                .keys
                .sorted()
                .compactMap { assignment[$0] }
                .joined(separator: ",")
        )
    }
}

Day21().solve()
