import AoCShared

struct Day10 : AdventOfCode {
    typealias InputData = [Int]
    
    func parse(input: String) -> [Int]? {
        let data = input.split(whereSeparator: \.isNewline).compactMap { Int(String($0)) }.sorted()
        return [0] + data + [data[data.count - 1] + 3]
    }
    
    func part1(data: [Int]) {
        var diffs: [Int: Int] = [:]
        
        for i in 1..<data.count {
            let d = data[i] - data[i - 1]
            if let dd = diffs[d] {
                diffs[d] = dd + 1
            } else {
                diffs[d] = 1
            }
        }
        
        if let d1 = diffs[1], let d3 = diffs[3] {
            print(d1 * d3)
        }
    }
    
    func part2(data: [Int]) {
        let end = data[data.count - 1]
        // Note that if data would count the value end-1 or end-2,
        // we would have to extend d with 1 or 2 elements
        var d = Array<Int>(repeating: 0, count: end + 1)
        d[end] = 1
        
        for n in data.dropLast().reversed() {
            d[n] = (1...3).map { d[n + $0] }.reduce(0) { $0 + $1 }
        }
        
        print(d[0])
    }
}

Day10().solve()
