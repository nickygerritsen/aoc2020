import AoCShared

struct Coordinate: Hashable {
    let x: Int
    let y: Int
    let z: Int
    let w: Int
}

struct State {
    var data: [Coordinate: Bool] = [:]
    
    mutating func process(withW: Bool) {
        var neighboursData: [Coordinate: Int] = [:]
        for c in data.keys {
            neighboursData[c] = 0
        }
        for c in data.keys {
            let neighbours = getNeighbourDiffs(withW: withW)
                .map { Coordinate(x: c.x + $0.x, y: c.y + $0.y, z: c.z + $0.z, w: c.w + $0.w) }
            for n in neighbours {
                neighboursData[n] = (neighboursData[n] ?? 0) + 1
            }
        }
        for (c, n) in neighboursData {
            if data[c] != nil {
                if n < 2 || n > 3 {
                    data[c] = nil
                }
            } else {
                if n == 3 {
                    data[c] = true
                }
            }
        }
    }
    
    func get(c: Coordinate) -> Bool {
        return data[c] ?? false
    }
    
    var activeCount: Int {
        return data
            .filter { $0.value }
            .count
    }
    
    func getNeighbourDiffs(withW: Bool) -> [Coordinate] {
        var diffs: [Coordinate] = []
        for x in -1...1 {
            for y in -1...1 {
                for z in -1...1 {
                    if withW {
                        for w in -1...1 {
                            diffs.append(Coordinate(x: x, y: y, z: z, w: w))
                        }
                    } else {
                        diffs.append(Coordinate(x: x, y: y, z: z, w: 0))
                    }
                }
            }
        }
        return diffs.filter { $0 != Coordinate(x: 0, y: 0, z: 0, w: 0) }
    }
}

struct Day17: AdventOfCode {
    typealias InputData = State
    let cycles = 6
    
    func parse(input: String) -> State? {
        var state = State()
        for (y, line) in input.split(whereSeparator: \.isNewline).enumerated() {
            for (x, char) in line.enumerated() {
                let c = Coordinate(x: x, y: y, z: 0, w: 0)
                if char == "#" {
                    state.data[c] = true
                }
            }
        }
        return state
    }
    
    func part1(data: State) {
        var d = data
        for _ in 1...cycles {
            d.process(withW: false)
        }
        print(d.activeCount)
    }
    
    func part2(data: State) {
        var d = data
        for _ in 1...cycles {
            d.process(withW: true)
        }
        print(d.activeCount)
    }
}

Day17().solve()
